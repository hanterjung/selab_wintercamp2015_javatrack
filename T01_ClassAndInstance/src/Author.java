import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanter on 2015-01-06.
 */
public class Author {
    private String name;

    private final List<Book> bookList;

    public Author(String name) {
        this.name = name;
        bookList = new ArrayList<Book>();
    }

    public String getName() {
        return name;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void writeBook(Book newBook) {
        bookList.add(newBook);
    }
}
