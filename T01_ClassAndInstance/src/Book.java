import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hanter on 2015-01-06.
 */
public class Book {
    private static final SimpleDateFormat PRINT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private final String title;
    private final Date datePublished;
    private int price;

    private final Publisher publisher;
    private final Author[] authors;
    private final ISBN isbn;
    private final List<Supplement> supplementList;

    private final List<BookItem> printBookItemList;

    public Book(String title, Date datePublished, int price,
                Publisher publisher, Author[] authors) {
        this.title = title;
        this.datePublished = datePublished;
        this.price = price;
        this.publisher = publisher;
        this.authors = authors;
        this.isbn = new ISBN(datePublished);

        printBookItemList = new ArrayList<BookItem>();
        supplementList = new ArrayList<Supplement>();
    }

    public String getTitle() {
        return title;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ISBN getIsbn() {
        return isbn;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public List<Supplement> getSupplementList() {
        return supplementList;
    }

    public void addSupplement(Supplement supplement) {
        supplementList.add(supplement);
    }

    public boolean removeSupplement(Supplement removeSupp) {
        for(Supplement supp : supplementList) {
            if(supp == removeSupp) {
                supplementList.remove(removeSupp);
                return true;
            }
        }
        return false;
    }

    public void addBookItem(BookItem bookItem) {
        printBookItemList.add(bookItem);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Title : ").append(title)
                .append("\nISBN10 : ").append(isbn.getISBN_10())
                .append("\nISBN13 : ").append(isbn.getISBN_13())
                .append("\nPublished Date : ").append(PRINT_DATE_FORMAT.format(datePublished))
                .append("\nPrice : $").append(price)
                .append("\nPublisher : ").append(publisher.getPublisherName())
                .append("\nAuthor : ");
        for(int i=0; i<authors.length; i++) {
            if(i != 0) sb.append(" and ");
            sb.append(authors[i].getName());
        }
        if(supplementList.size() > 0) {
            sb.append("\nSupplement : ");
            for(int i=0; i<supplementList.size(); i++) {
                if(i != 0) sb.append(" and ");
                sb.append(supplementList.get(i).getName());
            }
        }
        sb.append('\n');
        return  sb.toString();
    }
}
