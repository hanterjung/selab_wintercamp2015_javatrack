import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hanter on 2015-01-06.
 */
public class BookItem {
    private static final SimpleDateFormat PRINT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static int idCount = 1;

    private final String id;
    private final Date datePrinted;
    private final Book theBook;

    public BookItem(Book theBook, Date datePrinted) {
        this.id = "" + idCount;
        this.theBook = theBook;
        this.datePrinted = datePrinted;
        idCount++;
    }

    public String getId() {
        return id;
    }

    public Date getDatePrinted() {
        return datePrinted;
    }

    public Book getBook() {
        return theBook;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book Title : ").append(theBook.getTitle())
                .append("\nAuthor : ");
        final Author[] authors = theBook.getAuthors();
        for(int i = 0; i < authors.length; i++) {
            if(i != 0) sb.append(" and ");
            sb.append(authors[i].getName());
        }
        sb.append("\nPrinted Date : ").append(PRINT_DATE_FORMAT.format(datePrinted))
                .append("\nID : ").append(id)
                .append('\n');
        return  sb.toString();
    }
}
