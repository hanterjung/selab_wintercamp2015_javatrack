import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hanter on 2015-01-06.
 */
public class ISBN {
    private static final long ISBN_10_ADD_VALUE = 1000000000L;
    private static final SimpleDateFormat ISBN_13_DATE_FORMAT = new SimpleDateFormat("MMdd");

    private static int isbnCount = 0;

    private final Date registrationDate;
    private final String ISBN_10;
    private final String ISBN_13;

    public ISBN(Date registrationDate) {
        this.registrationDate = registrationDate;

        ISBN_10 = "" + (isbnCount + ISBN_10_ADD_VALUE);
        ISBN_13 = ISBN_13_DATE_FORMAT.format(registrationDate) +
                String.format("%09d", isbnCount);
        isbnCount++;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public String getISBN_10() {
        return ISBN_10;
    }

    public String getISBN_13() {
        return ISBN_13;
    }
}
