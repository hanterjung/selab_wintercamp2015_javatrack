import java.util.LinkedList;
import java.util.List;

/**
 * Created by hanter on 2015-01-06.
 */
public class Inventory {
    private final List<BookItem> bookItemList;

    public Inventory() {
        this.bookItemList = new LinkedList<BookItem>();
    }

    public void putBookItem(BookItem bookItem) {
        bookItemList.add(bookItem);
    }

    public BookItem takeOutBookItem(BookItem bookItem) {
        for(BookItem item : bookItemList) {
            if(item == bookItem) {
                bookItemList.remove(item);
                return item;
            }
        }
        return null;
    }

    public BookItem takeOutBookItem(Book matchedBook) {
        for(BookItem item : bookItemList) {
            if(item.getBook() == matchedBook) {
                bookItemList.remove(item);
                return item;
            }
        }
        return null;
    }

    public List<BookItem> getBookItemList() {
        return bookItemList;
    }
}
