import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanter on 2015-01-06.
 */
public class Publisher {
    private String publisherName;
    private String address;

    private final List<Book> bookList;

    public Publisher(String publisherName, String address) {
        this.publisherName = publisherName;
        this.address = address;

        bookList = new ArrayList<Book>();
    }

    public String getPublisherName() {
        return publisherName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void publishBook(Book newBook) {
        bookList.add(newBook);
    }
}
