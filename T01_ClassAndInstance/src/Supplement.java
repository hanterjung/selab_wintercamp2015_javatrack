/**
 * Created by hanter on 2015-01-06.
 */
public class Supplement {
    private final String name;
    private int price;

    public Supplement(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

