import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.System.out;

/**
 * Created by hanter on 2015-01-06.
 */
public class TestApp {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) throws ParseException {
        Author authorCay = new Author("Cay S. Horstmann");
        Author authorGray = new Author("Gray Cornell");

        Publisher pubPrentice = new Publisher("Prentice Hall",
                "Upper Saddle River, New Jersey 07458");

        Book bookCoreJava1 = publishNewBook("Core Java Volume 1.Fundamentals",
                DATE_FORMAT.parse("2012-12-07"), 34,
                pubPrentice, new Author[]{authorCay, authorGray});
        Book bookCoreJava2 =publishNewBook("Core Java Volume 2.Advanced Features",
                DATE_FORMAT.parse("2013-03-06"), 37,
                pubPrentice, new Author[]{authorCay, authorGray});

        Supplement supplementHFJava = new Supplement("Head First Java, 2nd Edition", 25);
        bookCoreJava2.addSupplement(supplementHFJava);


        Inventory inventory = new Inventory();

        printBookItem(inventory, bookCoreJava1, DATE_FORMAT.parse("2014-12-24"));
        printBookItem(inventory, bookCoreJava1, DATE_FORMAT.parse("2014-12-25"));
        printBookItem(inventory, bookCoreJava1, DATE_FORMAT.parse("2014-12-31"));
        printBookItem(inventory, bookCoreJava2, DATE_FORMAT.parse("2015-01-05"));
        printBookItem(inventory, bookCoreJava2, DATE_FORMAT.parse("2015-01-06"));
        printBookItem(inventory, bookCoreJava2, DATE_FORMAT.parse("2015-01-07"));


        out.println("====== BOOK List ======");
        out.println(bookCoreJava1);
        out.println(bookCoreJava2);

        out.println();
        out.println("====== BOOKITEM LIST ======");
        for(BookItem item : inventory.getBookItemList()) {
            out.println(item);
        }
    }


    //test helper methods
    public static Book publishNewBook(String title, Date datePublished, int price,
                                      Publisher publisher, Author[] authors) {
        Book newBook = new Book(title, datePublished, price, publisher, authors);
        for(Author author : authors)
            author.writeBook(newBook);
        publisher.publishBook(newBook);
        return newBook;
    }

    public static BookItem printBookItem(Inventory inventory, Book book, Date date) {
        BookItem item = new BookItem(book, date);
        book.addBookItem(item);
        inventory.putBookItem(item);
        return item;
    }
}
