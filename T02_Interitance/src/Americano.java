/**
 * Created by hanter on 2015-01-07.
 */
public class Americano extends Coffee {
    private int numberOfShot;

    {
        numberOfShot = 1;
    }

    public Americano(String name, int price) {
        super(name, price);
    }

    public Americano(String name, int price, int amountSyrup, int amountIce) {
        super(name, price, amountSyrup, amountIce);
    }

    public Americano(String name, int price, int amountSyrup, int amountIce, int numberOfShot) {
        super(name, price, amountSyrup, amountIce);
        this.numberOfShot = numberOfShot;
    }

    public int getShot() {
        return numberOfShot;
    }

    public void addAdditionalShot(int additionalShot) {
        numberOfShot += additionalShot;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append("\nNumber Of Shot : ").append(numberOfShot);
        return sb.toString();
    }
}
