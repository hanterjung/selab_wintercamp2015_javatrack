import java.util.ArrayList;

/**
 * Created by hanter on 2015-01-07.
 */
public abstract class Beverage {
    private static ArrayList<Beverage> beverageList;
    private static int numberOfBeverage;

    private String name;
    private int price;

    public Beverage(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name : ").append(name)
                .append("\nPrice : ₩").append(price);
        return sb.toString();
    }

    //static method
    static {
        beverageList = new ArrayList<Beverage>();
        numberOfBeverage = 0;
    }

    public static void add(Beverage beverage) {
        beverageList.add(beverage);
        numberOfBeverage = beverageList.size();
    }

    public static void remove(Beverage beverage) {
        beverageList.remove(beverage);
        numberOfBeverage = beverageList.size();
    }

    public static ArrayList<Beverage> getBeverageList() {
        return beverageList;
    }

    public static int getNumberOfBeveage() {
        return numberOfBeverage;
    }

    public static String makeBeverageListString() {
        StringBuilder sb = new StringBuilder();
        for(Beverage beverage : beverageList) {
            sb.append(beverage).append("\n\n");
        }
        return sb.toString();
    }
}
