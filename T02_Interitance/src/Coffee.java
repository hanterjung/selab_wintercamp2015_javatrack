/**
 * Created by hanter on 2015-01-07.
 */
public abstract class Coffee extends Beverage {
    private int amountSyrup;
    private int amountIce;

    public Coffee(String name, int price) {
        super(name, price);
        amountSyrup = 0;
        amountIce = 0;
    }

    public Coffee(String name, int price, int amountSyrup, int amountIce) {
        this(name, price);
        addSyrup(amountSyrup);
        addIce(amountIce);
    }

    public int getSyrupAmount() {
        return amountSyrup;
    }

    public int getIceAmount() {
        return amountIce;
    }

    public void addSyrup(int addAmountSyrup) {
        amountSyrup += addAmountSyrup;
    }

    public void addIce(int addAmountIce) {
        amountIce += addAmountIce;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append("\nAmount of Syrup : ").append(amountSyrup)
                .append("\nAmount of Ice : ").append(amountIce);
        return sb.toString();
    }
}
