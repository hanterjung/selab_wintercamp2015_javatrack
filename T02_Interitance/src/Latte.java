/**
 * Created by hanter on 2015-01-07.
 */
public class Latte extends Coffee {
    private int amountMilk;

    {
        amountMilk = 1;
    }

    public Latte(String name, int price) {
        super(name, price);
    }

    public Latte(String name, int price, int amountMilk) {
        super(name, price);
        this.amountMilk = amountMilk;
    }

    public Latte(String name, int price, int amountSyrup, int amountIce) {
        super(name, price, amountSyrup, amountIce);
    }

    public Latte(String name, int price, int amountSyrup, int amountIce, int amountMilk) {
        super(name, price, amountSyrup, amountIce);
        this.amountMilk = amountMilk;
    }

    public int getMilkAmount() {
        return amountMilk;
    }

    public void addMilk(int addAmountMilk) {
        amountMilk += addAmountMilk;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append("\nAmount of Milk : ").append(amountMilk);
        return sb.toString();
    }
}
