/**
 * Created by hanter on 2015-01-07.
 */
public class Milk extends Beverage {
    private int howMuchHeat;

    public Milk(String name, int price) {
        super(name, price);
        this.howMuchHeat = 0;
    }

    public Milk(String name, int price, int howMuchHeat) {
        super(name, price);
        this.howMuchHeat = howMuchHeat;
    }

    public int getHeat() {
        return howMuchHeat;
    }

    public void heatMilk(int addHeatAmount) {
        howMuchHeat += addHeatAmount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append("\nHow Much Heat : ").append(howMuchHeat);
        return sb.toString();
    }
}
