import java.util.ArrayList;
import java.util.List;

/**
 * Created by hanter on 2015-01-07.
 */
public class Tea extends Beverage {
    private List<String> teaBagList;

    public Tea(String name, int price) {
        super(name, price);
        teaBagList = new ArrayList<String>();
    }

    public Tea(String name, int price, String ... teaBags) {
        this(name, price);
        for(String teaBag : teaBags) {
            teaBagList.add(teaBag);
        }
    }

    public List<String> getTeaBagList() {
        return teaBagList;
    }

    public void addTeaBag(String teaBagName) {
        teaBagList.add(teaBagName);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString())
                .append("\nTeaBags : ");
        for(int i=0; i<teaBagList.size(); i++) {
            if(i!=0) sb.append(" & ");
            sb.append(teaBagList.get(i));
        }
        return sb.toString();
    }
}
