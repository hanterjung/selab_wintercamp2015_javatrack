import java.util.Random;

/**
 * Created by hanter on 2015-01-07.
 */
public class TestApp {
        public static void main(String[] args) {
        Random random = new Random(); //for executes methods

        //add Beverage
        Beverage.add(new Americano("Americano Hot", 3500));
        Beverage.add(new Americano("Americano Iced", 4000, 1, 10));
        Beverage.add(new Latte("Cafe Latte", 4000, 3));
        Beverage.add(new Milk("Heated Milk", 3500, 8));
        Beverage.add(new Tea("Mixed Tea", 4500, "Chamomile", "Earl Grey"));

        //test to execute each Subclass' methods
        for(Beverage beverage : Beverage.getBeverageList()) {
            if(beverage instanceof Americano) {
                Americano americano = (Americano)beverage;
                americano.addAdditionalShot(random.nextInt(3)+1);

            } else if(beverage instanceof Latte) {
                Latte latte = (Latte)beverage;
                latte.addMilk(random.nextInt(2)+1);

            } else if(beverage instanceof Milk) {
                Milk milk = (Milk)beverage;
                milk.heatMilk(random.nextInt(6)+1);

            } else if(beverage instanceof Tea) {
                Tea tea = (Tea)beverage;
                int teaType = random.nextInt(3);
                switch (teaType) {
                    case 0:
                        tea.addTeaBag("Rose Hips Oil");
                        break;
                    case 1:
                        tea.addTeaBag("Peppermint");
                        break;
                    case 2:
                        tea.addTeaBag("Chrysanthemum Tea");
                        break;
                }
            }
        }

        //print test result
        System.out.println(Beverage.makeBeverageListString());
    }
}
