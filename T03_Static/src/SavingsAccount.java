/**
 * Created by hanter on 2015-01-08.
 */
public class SavingsAccount {
    private static double annualInterestRate;

    private double savingsBalance;     //amount the saver currently has on deposit.


    public SavingsAccount(double initialSavingsBalance) {
        this.savingsBalance = initialSavingsBalance;
        floorCents(savingsBalance);
    }

    public double getBalance() {
        return savingsBalance;
    }

    public void deposit(double amount) {
        amount = floorCents(amount);
        if(amount < 0.0) amount = 0.0;

        savingsBalance += amount;
        savingsBalance = floorCents(savingsBalance);
    }

    public double withdraw(double amount) {
        amount = floorCents(amount);
        if(amount > savingsBalance) amount = savingsBalance;
        if(amount < 0.0) amount = 0.0;

        savingsBalance -= amount;
        savingsBalance = floorCents(savingsBalance);
        return amount;
    }

    public double calculateMonthlyInterest() {
        double monthlyInterest = savingsBalance * (annualInterestRate/12.0);
        savingsBalance += monthlyInterest; //auto added
        floorCents(savingsBalance);
        return monthlyInterest;
    }

    @Override
    public String toString() {
        return String.format("Balance : $%.2f", savingsBalance);
    }


    public static double getAnnualInterestRate() {
        return annualInterestRate;
    }

    //annualInterestRate is 1 == 100%
    public static void modifyInterestRate(double annualInterestRate) {
        if(annualInterestRate < 0.0) annualInterestRate = 0.0;
        SavingsAccount.annualInterestRate = annualInterestRate;
    }


    //센트 값 이하 소수점자릿수 버림(X.XX 3째 자리수 이하)
    private static double floorCents(double amount) {
        return (int)(amount * 100.0) / 100.0;
    }
}
