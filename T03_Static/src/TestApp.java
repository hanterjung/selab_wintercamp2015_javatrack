/**
 * Created by hanter on 2015-01-08.
 */
public class TestApp {
    public static void main(String[] args) {
        SavingsAccount saver1 = new SavingsAccount(2000.0);
        SavingsAccount saver2 = new SavingsAccount(3000.0);

        SavingsAccount.modifyInterestRate(0.04);

        System.out.println("###### Original ######");
        System.out.println("Saver1 - " + saver1);
        System.out.println("Saver2 - " + saver2);
        System.out.println();

        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();
        System.out.println("###### One month later ######");
        System.out.println("Saver1 - " + saver1);
        System.out.println("Saver2 - " + saver2);
        System.out.println();

        SavingsAccount.modifyInterestRate(0.05);
        saver1.calculateMonthlyInterest();
        saver2.calculateMonthlyInterest();
        System.out.println("###### Two month later ######");
        System.out.println("Saver1 - " + saver1);
        System.out.println("Saver2 - " + saver2);
    }
}
