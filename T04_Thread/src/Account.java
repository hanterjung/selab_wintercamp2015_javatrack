import java.util.Random;

/**
 * Created by hanter on 2015-01-09.
 */
public class Account {
    private static int accountCount = 0;
    private static Random random = new Random();

    private final int accountId;
    private int savingsBalance;

    private boolean bAccessing = false;


    public Account(int initialSavingsBalance) {
        this.savingsBalance = initialSavingsBalance;
        accountCount++;
        accountId = accountCount;
    }

    public int getID() {
        return accountId;
    }

    /**
     * Balance inquiry
     * @return the saving balance. if fail inquiring balance, return -1
     */
    public synchronized int getBalance() {
        try {
            while (bAccessing)
                wait();
            bAccessing = true;

            return savingsBalance;

        } catch (InterruptedException e) {
            e.printStackTrace();
            return -1;  //if fail inquiry

        } finally {
            bAccessing = false;
        }
    }

    /**
     * Deposit
     * @return OperationResult, null to fail
     */
    public synchronized OperationResult deposit(int amount) {
        int originalBalance = -1;   //for using recovery when occur InterruptedException

        try {
            while (bAccessing)
                wait();
            bAccessing = true;

            OperationResult result = new OperationResult(savingsBalance);
            try { Thread.sleep(getNextRndProcessTime()); }
            catch (InterruptedException e) {}

            if (amount < 0) amount = 0;
            originalBalance = savingsBalance;
            savingsBalance += amount;
            result.done(amount, savingsBalance);
            return result;

        } catch (InterruptedException e) {
            e.printStackTrace();
            if(originalBalance != -1)   //recover balance
                savingsBalance = originalBalance;
            return null;

        } finally {
            bAccessing = false;
            notifyAll();
        }
    }

    /**
     * Withdraw
     * @return OperationResult, null to fail
     */
    public synchronized OperationResult withdraw(int amount) {
        int originalBalance = -1;

        try {
            while (bAccessing)
                wait();
            bAccessing = true;

            OperationResult result = new OperationResult(savingsBalance);
            try { Thread.sleep(getNextRndProcessTime()); }
            catch (InterruptedException e) {}

            if (amount > savingsBalance) amount = savingsBalance;
            if (amount < 0) amount = 0;
            originalBalance = savingsBalance;
            savingsBalance -= amount;
            result.done(amount, savingsBalance);
            return result;

        } catch (InterruptedException e) {
            e.printStackTrace();
            if(originalBalance != -1)   //recover balance
                savingsBalance = originalBalance;
            return null;

        } finally {
            bAccessing = false;
            notifyAll();
        }
    }

    /**
     * Withdraw
     * @return success : amount of transfer (in balance limit)
     *         failure : -1
     */
    public synchronized TransferResult transfer(Account toAccount, int amount) {
        TransferResult transferResult = new TransferResult(getBalance());

        OperationResult withdrawResult = withdraw(amount);
        if(withdrawResult == null) {
            return null;      //failure
        }

        OperationResult depositResult = toAccount.deposit(withdrawResult.amount);
        if(depositResult != null) {  //success
            transferResult.done(withdrawResult.getAmount(), withdrawResult.afterBalance,
                    depositResult.beforeBalance, depositResult.afterBalance);
            return transferResult;
        }

        //failure -> recovery withdraw -> deposit
        //try to deposit until to succeed
        while(deposit(withdrawResult.amount) != null);
        return null;
    }

    @Override
    public String toString() {
        return String.format("AccountId:%d, Balance:￦%d", accountId, getBalance());
    }


    private static int getNextRndProcessTime() {
        return (random.nextInt(5) + 1) * 1000; //1~5s
    }


    public class OperationResult {
        private int amount;
        private long startTime;
        private long operationTime;
        private int beforeBalance;
        private int afterBalance;

        private OperationResult(int beforeBalance) {
            this.startTime = System.currentTimeMillis();
            this.beforeBalance = beforeBalance;
        }

        private void done(int amount, int afterBalance) {
            this.amount = amount;
            this.operationTime = System.currentTimeMillis() - startTime;
            this.afterBalance = afterBalance;
        }

        public int getAmount() {
            return amount;
        }

        public long getOperationTime() {
            return operationTime;
        }

        public int getBeforeBalance() {
            return beforeBalance;
        }

        public int getAfterBalance() {
            return afterBalance;
        }
    }

    public class TransferResult extends OperationResult {
        private int targetBeforeBalance;
        private int targetAfterBalance;

        private TransferResult(int beforeBalance) {
            super(beforeBalance);
        }

        private void done(int amount, int afterBalance,
                          int targetBeforeBalance, int targetAfterBalance) {
            super.done(amount, afterBalance);
            this.targetBeforeBalance = targetBeforeBalance;
            this.targetAfterBalance = targetAfterBalance;
        }

        public int getTargetBeforeBalance() {
            return targetBeforeBalance;
        }

        public int getTargetAfterBalance() {
            return targetAfterBalance;
        }
    }
}
