import java.security.InvalidParameterException;

/**
 * Created by hanter on 2015-01-09.
 */
public class Request implements Comparable<Request> {
    public static final int REQUEST_TYPE_DEPOSIT = 0;
    public static final int REQUEST_TYPE_WITHDRAW = 1;
    public static final int REQUEST_TYPE_TRANSFER = 2;
    public static final int REQUEST_TYPES_COUNT = 3;

    private static int requestCount = 0;

    private final int requestNumber;
    private final int requestType;
    private final int amount;
    private final Account fromAccount;
    private final Account toAccount;

    private long startTime = 0l;
    private long executeTime = 0l;
    private boolean bDone = false;

    public Request(int type, int amount, Account from, Account to)
            throws InvalidParameterException {
        //Valid Test
        switch (type) {
            case REQUEST_TYPE_DEPOSIT:
            case REQUEST_TYPE_WITHDRAW:
                if(from == null)
                    throw new InvalidParameterException("fromAccount MUST be not null");
                break;

            case REQUEST_TYPE_TRANSFER:
                if(from == null || to == null)
                    throw new InvalidParameterException("from/toAccount MUST be not null");
                break;

            default:
                throw new InvalidParameterException("WRONG request type");
        }

        this.requestType = type;
        this.amount = amount;
        this.fromAccount = from;
        this.toAccount = to;

        requestCount++;
        requestNumber = requestCount;
    }

    public void executeBegin() {
        startTime = System.currentTimeMillis();
    }

    public void executeDone() {
        executeTime = System.currentTimeMillis() - startTime;
        bDone = true;
    }

    @Override
    public int compareTo(Request another) {
        //먼저올수록 requestNumber가 낮음 => 높은 우선순위
        return this.requestNumber - another.requestNumber;
    }

    public int getRequestNumber() {
        return requestNumber;
    }

    public int getRequestType() {
        return requestType;
    }

    public int getAmount() {
        return amount;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public boolean isDone() {
        return bDone;
    }

    public long getExecuteTime() {
        return executeTime;
    }
}
