import java.util.concurrent.BlockingQueue;

/**
 * Created by hanter on 2015-01-09.
 */
public class Teller implements Runnable {
    private static int tellerCount = 0;

    private final int tellerId;

    private BlockingQueue<Request> requestQueue;
    private Request nowRequest = null;
    private boolean bWorkingTime;

    private RequestDoneListener requestDoneListener = null;


    public Teller() {
        tellerCount++;
        tellerId = tellerCount;
        bWorkingTime = true;
    }

    @Override
    public void run() {
        while(bWorkingTime) {
            if(nowRequest == null) {    //take request in queue
                takeRequest();
            }

            if(nowRequest != null) {    //has request already
                acceptRequest();
            }

            if(requestQueue.isEmpty())

            try { Thread.sleep(10); }
            catch (InterruptedException e) {}
        }
    }

    private void acceptRequest() {
        int requestFailureCount = 0;

        while(requestFailureCount<3) {
            String result = executeRequest();
            if(result != null) {
                requestDoneListener.onRequestDone(this, nowRequest, result);
                nowRequest = null;
                return;
            }

            requestFailureCount++;
        }

        //Teller에서 3번 이상 시도해서 실패하면 더이상 처리 안하고 반환
        if(requestDoneListener != null) {
            String requestName = "";
            switch (nowRequest.getRequestType()) {
                case Request.REQUEST_TYPE_DEPOSIT:
                    requestName = "Deposit";
                    break;
                case Request.REQUEST_TYPE_WITHDRAW:
                    requestName = "Withdraw";
                    break;
                case Request.REQUEST_TYPE_TRANSFER:
                    requestName = "Transfer";
                    break;
            }

            requestDoneListener.onRequestFailed(this, nowRequest, "Failure");
            nowRequest = null;
        }
    }

    private String executeRequest() {
        switch (nowRequest.getRequestType()) {
            case Request.REQUEST_TYPE_DEPOSIT:
                Account.OperationResult depositResult
                        = nowRequest.getFromAccount().deposit(nowRequest.getAmount());
                if(depositResult != null) {
                    return String.format("%d, Person %d, Deposit:%d, Teller %d, %d, %d",
                            depositResult.getOperationTime(), nowRequest.getFromAccount().getID(),
                            depositResult.getAmount(), tellerId,
                            depositResult.getBeforeBalance(), depositResult.getAfterBalance());
                }

            case Request.REQUEST_TYPE_WITHDRAW:
                Account.OperationResult withdrawResult
                        = nowRequest.getFromAccount().deposit(nowRequest.getAmount());
                if(withdrawResult != null ) {
                    return String.format("%d, Person %d, WithDraw:%d, Teller %d, %d, %d",
                            withdrawResult.getOperationTime(), nowRequest.getFromAccount().getID(),
                            withdrawResult.getAmount(), tellerId,
                            withdrawResult.getBeforeBalance(), withdrawResult.getAfterBalance());
                }

            case Request.REQUEST_TYPE_TRANSFER:
                Account.TransferResult transferResult
                        = nowRequest.getFromAccount().transfer(nowRequest.getToAccount(),
                                nowRequest.getAmount());
                if(transferResult != null) {
                    return String.format("%d, Person %d, WithDraw:%d, Teller %d, %d, %d, (Target)%d, %d",
                            transferResult.getOperationTime(), nowRequest.getFromAccount().getID(),
                            transferResult.getAmount(), tellerId,
                            transferResult.getBeforeBalance(), transferResult.getAfterBalance(),
                            transferResult.getTargetBeforeBalance(),
                            transferResult.getTargetAfterBalance());
                }
        }

        return null;
    }

    private void takeRequest() {
        try {
            nowRequest = requestQueue.take();
        } catch (InterruptedException e) {
            nowRequest = null;
        }
    }

    public void setRequestQueue(BlockingQueue<Request> queue) {
        requestQueue = queue;
    }

    public boolean hasRequest() {
        return (nowRequest != null);
    }

    public void getOffWork() {  //퇴근!!!
        bWorkingTime = false;
    }

    public boolean isWorking() {
        return bWorkingTime;
    }

    public int getID() {
        return tellerId;
    }

    public void setRequestDoneListener(RequestDoneListener l) {
        this.requestDoneListener = l;
    }

    public interface RequestDoneListener {
        public void onRequestDone(Teller teller, Request succeedRequest, String doneMessage);
        public void onRequestFailed(Teller teller, Request failedRequest, String failMessage);
    }
}
