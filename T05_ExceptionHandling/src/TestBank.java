import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by hanter on 2015-01-09.
 */
public class TestBank {
    public static void main(String args[]) {
        TestBank bank = new TestBank();

        for(int i=0; i<100; i++) {
            bank.addRandomAccount();
        }
        for(int i=0; i<100; i++) {
            bank.addRandomRequest(i);
        }

        for(int i=0; i<3; i++) {
            bank.addTeller();
        }

        bank.startTellerJob();
    }

    private ArrayList<Account> accountList;
    private PriorityBlockingQueue<Request> requestQueue;
    private ArrayList<Teller> tellerList;

    private int requestDoneCount;
    private Random random;

    private BufferedWriter resultFileWriter = null;

    public TestBank() {
        accountList = new ArrayList<Account>();
        requestQueue = new PriorityBlockingQueue<Request>();
        tellerList = new ArrayList<Teller>();

        requestDoneCount = 0;
        random = new Random();

        try {
            resultFileWriter = new BufferedWriter(new FileWriter("result.txt"));
        } catch (Exception e) {
            e.printStackTrace();
            resultFileWriter = null;
        }
    }

    public void startTellerJob() {
        for(Teller teller : tellerList) {
            teller.setRequestQueue(requestQueue);
        }

        for(Teller teller : tellerList) {
            new Thread(teller).start();
        }
    }

    public boolean isEndedAllTeller() {
        for(Teller teller : tellerList) {
            if(teller.isWorking()) return false;
        }
        return true;
    }

    public void addTeller() {
        Teller teller = new Teller();
        teller.setRequestDoneListener(requestDoneListener);

        tellerList.add(teller);
    }

    public void addAccount(Account account) {
        accountList.add(account);
    }

    public void addRandomAccount() {
        addAccount(new Account((random.nextInt(10)+1) *
                               (int)(Math.pow(10, random.nextInt(3)+4))));
    }

    public void addRequest(Request request) {
        requestQueue.put(request);
    }

    public void addRandomRequest(int accountNumber) {
        int requestType = random.nextInt(Request.REQUEST_TYPES_COUNT);
        Account fromAccount = accountList.get(accountNumber);
        Account toAccount = null;
        int amount = (random.nextInt(10)+1) *
                     (int)(Math.pow(10, random.nextInt(2)+3));

        if(requestType == Request.REQUEST_TYPE_TRANSFER) {
            int toAccountNum = 0;
            //from과 to가 달라야 함
            while((toAccountNum=random.nextInt(accountList.size())) == accountNumber);
            toAccount = accountList.get(toAccountNum);
        }

        addRequest(new Request(requestType, amount, fromAccount, toAccount));
    }




    private Teller.RequestDoneListener requestDoneListener = new Teller.RequestDoneListener() {
        @Override
        public synchronized void onRequestDone(Teller teller,
                                               Request succeedRequest, String doneMessage) {
            String resultString = requestDoneCount + ", " + doneMessage;
            requestDoneCount++;

            System.out.println(resultString);
            try {
                synchronized (resultFileWriter) {
                    resultFileWriter.append(resultString).append("\r\n");
                    resultFileWriter.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(requestQueue.isEmpty()) {
                teller.getOffWork();
            }

            if(isEndedAllTeller()) {
                try {
                    resultFileWriter.close();
                } catch (IOException e) {}
                System.out.println("Done!");
            }
        }

        @Override
        public synchronized void onRequestFailed(Teller teller,
                                                 Request failedRequest, String failMessage) {
            //큐에 다시 집어 넣음 - 자신 또는 다른 Teller가 다시 가져가서 처리를 시도할 수 있도록 함.
            //System.out.println(failMessage);
            requestQueue.put(failedRequest);
        }
    };
}
