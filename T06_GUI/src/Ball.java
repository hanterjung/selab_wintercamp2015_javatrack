import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by hanter on 2015. 1. 19..
 */
public class Ball {
    private double ballSize;
    private double xPos, yPos;
    private double xSpeed, ySpeed;
    private boolean isMoving;

    public Ball(double ballSize, double xPos, double yPos, double xSpeed, double ySpeed) {
        this.ballSize = ballSize;
        this.xPos = xPos;
        this.yPos = yPos;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.isMoving = false;
    }

    public void setMoving(boolean bMoving) {
        this.isMoving = bMoving;
    }

    public void move(Rectangle2D bounds) {
        if(!isMoving) return;

        xPos += xSpeed;
        yPos += ySpeed;
        if(xPos <= 0) {
            xPos = 0;
            xSpeed *= -1;
        }
        if(xPos + ballSize >= bounds.getWidth()) {
            xPos = bounds.getWidth() - ballSize;
            xSpeed *= -1;
        }
        if(yPos <= 0) {
            yPos = 0;
            ySpeed *= -1;
        }
        if(yPos + ballSize >= bounds.getHeight()) {
            yPos = bounds.getHeight() - ballSize;
            xSpeed *= -1;
        }
    }

    public Ellipse2D getShpae() {
        return new Ellipse2D.Double(xPos, yPos, ballSize, ballSize);
    }
}
