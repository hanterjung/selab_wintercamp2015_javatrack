import javax.swing.*;
import java.awt.*;

/**
 * Created by hanter on 2015. 1. 19..
 */
public class BallPanel extends JPanel {
    private Ball ball = null;

    public void setBall(Ball ball) {
        this.ball = ball;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;

        if(ball != null)
            g2d.fill(ball.getShpae());
    }

}
