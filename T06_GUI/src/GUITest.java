import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by hanter on 2015. 1. 19..
 */
public class GUITest extends JFrame {
    public static void main(String args[]) {
        GUITest guiFrame = new GUITest();
    }

    private Ball ball = null;
    private BallThread ballThread;

    private BallPanel panelBall;    //layer1
    private JPanel panelButtons;    //layer2
    private JButton btnStart, btnStop;      //buttons


    public GUITest() throws HeadlessException {
        super("GUI Ball Test");
        makeFrame();

        ballThread = new BallThread();
        ballThread.start();
    }

    private void startBall() {
        if(ball == null) {
            System.out.println("make ball!");
            ball = new Ball(15, 10, 10, 3, 0);
            panelBall.setBall(ball);
        }

        System.out.println("start ball!");
        ball.setMoving(true);
    }

    private void stopBall() {
        System.out.println("stop ball!");
        ball.setMoving(false);
    }

    private void makeFrame() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMaximumSize(new Dimension(600, 500));
        setMinimumSize(new Dimension(600, 500));
        setSize(600, 500);
        setLayout(null);

        panelBall = new BallPanel();
        panelBall.setBounds(10, 10, 580, 380);
        panelBall.setBorder(BorderFactory.createTitledBorder(""));
        add(panelBall);

        panelButtons = new JPanel(null);
        panelButtons.setBounds(10, 400, 580, 70);
        panelButtons.setBorder(BorderFactory.createTitledBorder(""));
        add(panelButtons);

        btnStart = new JButton("Start");
        btnStart.setBounds(50, 10, 200, 50);
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startBall();
            }
        });
        panelButtons.add(btnStart);

        btnStop = new JButton("Stop");
        btnStop.setBounds(330, 10, 200, 50);
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stopBall();
            }
        });
        panelButtons.add(btnStop);

        setVisible(true);
    }


    private class BallThread extends Thread {
        private static final int DELAY = 30;

        @Override
        public void run() {
            while (true) {
                if(ball != null) ball.move(panelBall.getBounds());
                panelBall.repaint();
                try { Thread.sleep(DELAY); }
                catch (InterruptedException ie) {}
            }
        }
    }
}
