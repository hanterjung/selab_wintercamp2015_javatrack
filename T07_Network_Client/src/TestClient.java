import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by hanter on 2015. 1. 16..
 */
public class TestClient {

    public static void main(String args[]) {
        TestClient client = new TestClient(12345);
        client.startClient();
    }


    private Socket socket;
    private int serverPort;

    private BufferedReader reader = null;
    private PrintWriter writer = null;

    public TestClient(int serverPort) {
        this.serverPort = serverPort;
    }

    public void startClient() {
        System.out.println("start client");
        while(true) {
            try {
                socket = new Socket(InetAddress.getLocalHost(), serverPort);
                break;
            } catch (Exception e) {
                try {
                    Thread.sleep(200);
                } catch (Exception e2) {}
                //retry serverSocket
                System.out.println("connection request fail, retry...");
            }
        }

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (Exception e) {
            System.out.println("failed to load stream... exit Client.");
            try {
                socket.close();
            } catch (Exception e2) {}
            return; //end
        }

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Select Method - 1:toUpperCase, 2:toLowerCase"
                    + ", 0:exit");
            System.out.println("input like - 1:Hello (method_number:MESSAGE)");
            System.out.print("input: ");
            String input = scanner.nextLine();

            if(input.startsWith("0")) {
                try {
                    reader.close();
                    writer.close();
                    //socket.close(); //서버에서 닫음
                } catch (Exception e) {}
                System.out.println("good bye~");
                return;
            }

            if(vaildInput(input)) {
                try {
                    writer.println(input);
                    writer.flush();
                    String result = reader.readLine();
                    System.out.println("result: " + result);
                } catch (IOException ioe) {
                    System.out.println("occur IOException!");
                }
            } else {
                System.out.println("this is not vaild input! please retry.");
            }
        }
    }

    private boolean vaildInput(String input) {
        if(input.startsWith("1:") || input.startsWith("2:")) return true;
        return false;
    }

}
