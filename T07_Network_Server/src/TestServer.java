import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by hanter on 2015. 1. 16..
 */
public class TestServer {

    public static void main(String args[]) {
        TestServer server = new TestServer(12345);
        server.startServer();
    }


    private ServerSocket serverSocket;
    private Socket socket;
    private int serverPort;

    private BufferedReader reader = null;
    private PrintWriter writer = null;

    public TestServer(int port) {
        serverPort = port;
    }

    public void startServer() {
        System.out.println("start server");
        while(true) {
            try {
                serverSocket = new ServerSocket(serverPort);
                socket = serverSocket.accept();
                System.out.println("socket connected.");
                break;
            } catch (Exception e) {
                try {
                    if (serverSocket != null) serverSocket.close();
                    Thread.sleep(200);
                } catch (Exception e2) {}
                //retry serverSocket
                System.out.println("socket connect fail, retry...");
            }
        }

        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (Exception e) {
            System.out.println("failed to load stream... exit Server.");
            try {
                socket.close();
                serverSocket.close();
            } catch (Exception e2) {}
            return; //end
        }

        while (true) {
            try {
                System.out.println("waiting to client's message...");
                String message = reader.readLine();
                if (message.startsWith("0")) {
                    try {
                        reader.close();
                        writer.close();
                        socket.close();
                        serverSocket.close();
                    } catch (Exception e) {
                    }
                    System.out.println("good bye~");
                    return;
                }

                System.out.println("client's message: " + message);
                String result = parseAndExecute(message);
                writer.println(result);
                writer.flush();
                System.out.println("send result: " + result);
            } catch (Exception e) {
                System.out.println("there is something wrong... exit server");
                return;
            }
        }
    }


    public String parseAndExecute(String message) {
        if(message.startsWith("1:")) {
            return message.substring(2).toUpperCase();
        } else if (message.startsWith("2:")) {
            return message.substring(2).toLowerCase();
        }
        return null;
    }
}
