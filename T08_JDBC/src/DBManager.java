import org.sqlite.SQLiteConfig;

import java.sql.*;

/**
 * Created by hanter on 2015. 1. 19..
 */
public class DBManager {
    public static final String DATABASE_PATH = "/sqlite/sel_students.db";

    private boolean bConnect = false;
    private Connection connection = null;

    public Connection connect() {
        if(bConnect) return connection;

        SQLiteConfig config = new SQLiteConfig();

        try {
            String uri = "jdbc:sqlite:" + System.getProperty("user.dir") + DATABASE_PATH;
            System.out.println("uri: " + uri);
            connection = DriverManager.getConnection(uri, config.toProperties());
            System.out.println("Driver found! Connection Good!");
            bConnect = true;
        } catch (SQLException sqle) {
            connection = null;
            sqle.printStackTrace();
            System.out.println("sql error");
        }

        return connection;
    }

    public void close() {
        try {
            if(connection != null) {
                connection.close();
                connection = null;
            }

        } catch (SQLException e) {
        } finally {
            try {
                if(connection != null) {
                    connection.close();
                    connection = null;
                }
            } catch (SQLException e2) {}
            connection = null;
            bConnect = false;
        }
    }

    public void closePreparedStatement(PreparedStatement pstmt, ResultSet rs) {
        try {
            if(rs!=null) { rs.close(); rs=null; }
            if(pstmt!=null) { pstmt.close(); pstmt=null; }
        } catch (SQLException e) {
        } finally {
            try {
                if(rs!=null) rs.close();
                if(pstmt!=null) pstmt.close();
            } catch (SQLException e) {}
        }
    }


    private DBManager() throws ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
    }

    private static DBManager instance = null;
    public static DBManager getSharedDBManager() {
        if(instance != null) return instance;

        try {
            instance = new DBManager();
            return instance;
        } catch (ClassNotFoundException cne) {
            System.out.println("sqlite jdbc driver not founded!");
            cne.printStackTrace();
            return null;
        }
    }

}
