import javax.swing.plaf.nimbus.State;
import java.sql.*;

/**
 * Created by hanter on 2015. 1. 19..
 */
public class JDBCTest {

    public static void main(String args[]) {
        JDBCTest testApp = new JDBCTest();
        if(!testApp.isDBLoaded()) return;

        try {
            testApp.createTables();

            testApp.insertStudent(1, "Dong Ha Shin", 26, "Male");
            testApp.insertStudent(2, "Seok Hyun Bae", 25, "Male");
            testApp.insertStudent(3, "Gil Su Lim", 25, "Male");
            testApp.insertStudent(4, "Han Ter Jung", 24, "Male");

            testApp.insertCourse(1, "Java", "Jae You Lee");
            testApp.insertCourse(2, "English Grammar", "Hyun Jung La");
            testApp.insertCourse(3, "504 Vocabulary", "Moon Kwon Kim");

            testApp.insertScore(1, 1, 99);
            testApp.insertScore(1, 2, 90);
            testApp.insertScore(2, 1, 92);
            testApp.insertScore(2, 2, 85);
            testApp.insertScore(2, 3, 90);
            testApp.insertScore(3, 2, 87);
            testApp.insertScore(3, 3, 89);
            testApp.insertScore(4, 1, 95);
            testApp.insertScore(4, 3, 92);

            System.out.println();
            testApp.printStudentTables();
            testApp.printCourseTables();
            testApp.printScoreTables();
            testApp.printStudentCourseGradeTables();

            testApp.updateScore(2, 3, 100);
            System.out.println("After Updated fifth row");
            testApp.printScoreTables();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static final String TABLE_NAME_STUDENT = "Student";
    public static final String TABLE_NAME_COURSE = "Course";
    public static final String TABLE_NAME_SCORE = "Score";

    private DBManager dbManager = null;

    public JDBCTest() {
        dbManager = DBManager.getSharedDBManager();
        if(dbManager == null) {
            System.out.println("NO SQLITE, PROGRAM TERMINATE!");
        } else {
            System.out.println("sqlite load success");
        }
    }

    public int insertStudent(int id, String name, int age, String sex) throws SQLException {
        final String studentInsert = "INSERT INTO " + TABLE_NAME_STUDENT
                + " (StudentId, Name, Age, Sex) VALUES (?, ?, ?, ?)";
        int result;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(studentInsert);

        pstmt.setInt(1, id);
        pstmt.setString(2, name);
        pstmt.setInt(3, age);
        pstmt.setString(4, sex);
        result = pstmt.executeUpdate();
        dbManager.closePreparedStatement(pstmt, null);
        return result;
    }

    public int insertCourse(int id, String courseName, String instructor) throws SQLException {
        final String courseInsert = "INSERT INTO " + TABLE_NAME_COURSE
                + " (CourseId, CourseName, Instructor) VALUES (?, ?, ?)";
        int result;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(courseInsert);

        pstmt.setInt(1, id);
        pstmt.setString(2, courseName);
        pstmt.setString(3, instructor);
        result = pstmt.executeUpdate();
        dbManager.closePreparedStatement(pstmt, null);
        return result;
    }

    public int insertScore(int studentId, int courseId, int grade) throws SQLException {
        final String scoreInsert = "INSERT INTO " + TABLE_NAME_SCORE
                + " (Student, Course, Grade) VALUES (?, ?, ?)";
        int result;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(scoreInsert);

        pstmt.setInt(1, studentId);
        pstmt.setInt(2, courseId);
        pstmt.setInt(3, grade);
        result = pstmt.executeUpdate();
        dbManager.closePreparedStatement(pstmt, null);
        return result;
    }

    public int updateScore(int studentId, int courseId, int grade) throws SQLException {
        final String scoreInsert = "UPDATE " + TABLE_NAME_SCORE
                + " SET Grade = ? WHERE Student = ? AND Course = ?";
        int result;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(scoreInsert);

        pstmt.setInt(1, grade);
        pstmt.setInt(2, studentId);
        pstmt.setInt(3, courseId);
        result = pstmt.executeUpdate();
        System.out.println("Update " + result + "row(s)\n");
        dbManager.closePreparedStatement(pstmt, null);
        return result;
    }

    public void printStudentTables() throws SQLException {
        final String studentSelectAll = "SELECT * FROM " + TABLE_NAME_STUDENT;
        ResultSet rs;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(studentSelectAll);

        System.out.println("===== Student Table =====");
        System.out.println(String.format("%-9s | %-16s | %-3s | %-6s",
                "StudentId", "Name", "Age", "Sex" ));
        rs = pstmt.executeQuery();
        while(rs.next()) {
            System.out.println(String.format("%9d | %-16s | %3d | %-6s",
                    rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
        }
        System.out.println();
        dbManager.closePreparedStatement(pstmt, null);
    }

    public void printCourseTables() throws SQLException {
        final String courseSelectAll = "SELECT * FROM " + TABLE_NAME_COURSE;
        ResultSet rs;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(courseSelectAll);

        System.out.println("===== Course Table =====");
        System.out.println(String.format("%-9s | %-16s | %-16s",
                "CourseId", "CourseName", "Instructor" ));
        rs = pstmt.executeQuery();
        while(rs.next()) {
            System.out.println(String.format("%9d | %-16s | %-16s",
                    rs.getInt(1), rs.getString(2), rs.getString(3)));
        }
        System.out.println();
        dbManager.closePreparedStatement(pstmt, null);
    }

    public void printScoreTables() throws SQLException {
        final String scoreSelectAll = "SELECT * FROM " + TABLE_NAME_SCORE;
        ResultSet rs;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(scoreSelectAll);

        System.out.println("===== Score Table =====");
        System.out.println(String.format("%-9s | %-9s | %-6s",
                "Student", "Course", "Grade"));
        rs = pstmt.executeQuery();
        while(rs.next()) {
            System.out.println(String.format("%9d | %9d | %6d",
                    rs.getInt(1), rs.getInt(2), rs.getInt(3)));
        }
        System.out.println();
        dbManager.closePreparedStatement(pstmt, null);
    }

    public void printStudentCourseGradeTables() throws SQLException {
        final String scoreSelectAll = "SELECT Name, CourseName, Grade FROM "
                + TABLE_NAME_STUDENT + "," + TABLE_NAME_COURSE + "," +TABLE_NAME_SCORE
                + " WHERE " + TABLE_NAME_STUDENT + ".StudentId"
                + "=" + TABLE_NAME_SCORE + ".Student"
                + " AND " + TABLE_NAME_COURSE + ".CourseID"
                + "=" + TABLE_NAME_SCORE + ".Course";
        ResultSet rs;
        Connection conn = dbManager.connect();
        PreparedStatement pstmt = conn.prepareStatement(scoreSelectAll);

        System.out.println("===== Student-Grade-Score Table =====");
        System.out.println(String.format("%-16s | %-16s | %-6s",
                "StudentName", "CourseName", "Grade"));
        rs = pstmt.executeQuery();
        while(rs.next()) {
            System.out.println(String.format("%-16s | %-16s | %6d",
                    rs.getString("Name"), rs.getString("CourseName"),
                    rs.getInt("Grade")));
        }
        System.out.println();
        dbManager.closePreparedStatement(pstmt, rs);
    }

    public void createTables() throws SQLException {
        Connection conn = dbManager.connect();
        Statement stmt = conn.createStatement();

        final String studentCreate = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_STUDENT
                + "( StudentId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "  Name TEXT NOT NULL,"
                + "  Age INTEGER NOT NULL,"
                + "  Sex TEXT NOT NULL)";
        stmt.execute(studentCreate);

        final String courseCreate = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_COURSE
                + "( CourseId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "  CourseName TEXT NOT NULL,"
                + "  Instructor TEXT)";
        stmt.execute(courseCreate);

        final String scoreCreate = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_SCORE
                + "( Student INTEGER NOT NULL,"
                + "  Course INTEGER NOT NULL,"
                + "  Grade INTEGER NOT NULL,"
                + "  FOREIGN KEY(Student) REFERENCES " + TABLE_NAME_STUDENT + "(StudentId),"
                + "  FOREIGN KEY(Course) REFERENCES " + TABLE_NAME_COURSE + "(CourseId),"
                + "  PRIMARY KEY (Student, Course))";
        stmt.execute(scoreCreate);
    }

    @Override
    protected void finalize() throws Throwable {
        dbManager.close();
        super.finalize();
    }

    public boolean isDBLoaded() {
        return (dbManager != null);
    }
}
